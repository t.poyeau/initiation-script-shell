#!/bin/bash
for PARAM in $@
do
    if [ -f $PARAM ]
    then
        chmod -v a+r $PARAM
    elif [ -d $PARAM ]
    then
        chmod -v a+x $PARAM
    else
        echo "Erreur : '$PARAM' n'est ni un fichier ordinaire, ni un répertoire" >&2
    fi
done
